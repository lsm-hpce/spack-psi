# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-hiqsimulator
#
# You can edit this file again by typing:
#
#     spack edit py-hiqsimulator
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *


class PyHiqsimulator(PythonPackage):
    """Huawei HiQ is an open-source software framework for quantum computing. It is based on and compatible with ProjectQ.
    It aims at providing tools which facilitate inventing, implementing, testing, debugging, and running quantum algorithms
    using either classical hardware or actual quantum devices. You can find more about Huawei HiQ at
    http://hiq.huaweicloud.com"""

    homepage = "https://hiqsimulator.readthedocs.io/en/latest/index.html"

    git = "https://github.com/Huawei-HiQ/HiQsimulator"

    maintainers = ["germanne"]

    version('develop', submodules=True)

    depends_on('py-setuptools@44.1.0', type='build')
    depends_on('py-mpi4py', type=("run"))
    depends_on('py-projectq@0.4.1', type=("run"))

    depends_on("cmake", type="build")
    depends_on("openmpi", type=("run", "link"))
    depends_on("boost+mpi+program_options+numpy+python+system+thread+timer+chrono+atomic", type=("build", "run", "link"))
    depends_on("glog", type=("build", "run", "link"))

    patch('cmake-glog.patch')

    def setup_build_environment(self, env):
        env.set('BOOST_ROOT', self.spec['boost'].prefix)

    def install_options(self, spec, prefix):
        # FIXME: Add options to pass to setup.py install
        # FIXME: If not needed, delete this function
        options = []
        return options
