# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install hyquas
#
# You can edit this file again by typing:
#
#     spack edit hyquas
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *

class Hyquas(CMakePackage, CudaPackage):
    """HyQuas is a Hybrid partitioner based Quantum circuit Simulation system on GPU, which supports both single-GPU,
    single-node-multi-GPU, and multi-node-multi-GPU quantum circuit simulation."""

    homepage = "https://github.com/thu-pacman/HyQuas#readme"
    git = "https://github.com/thu-pacman/HyQuas.git"

    maintainers = ["germanne"]

    version('main', submodules=True)

    depends_on("cmake", type=('build'))
    depends_on("mpi", type=('link', 'run'))

    for value in CudaPackage.cuda_arch_values:
        depends_on("nccl@2.9.6-1+cuda cuda_arch={0}".format(value), type=('link', 'run'), when='cuda_arch={0}'.format(value))

    variant("cuda", default=True, description="Build with CUDA")
    variant("backend", default='blas', description="Build with backend type:", values=('serial', 'group', 'group-serial', 'blas', 'mix', 'blas-advance'))
    variant("schedule", default=True, description="Build with printing the schedule")
    variant("summary", default=True, description="Show the running details")
    variant("measure_stage", default=False, description="Measure time of each stage")
    variant("micro_bench", default=False, description="Compile micro-benchmarks")
    variant("eval_pp", default=False, description="Compile evaluator preprocess")
    variant("disable_assert", default=True, description="Use assert in cuda runtime")
    variant("use_double", default=True, description="Double or float precision")
    variant("overlap", default=True, description="Enable overlap")
    variant("mpi", default=False, description="Use mpi")
    variant("overlap_mat", default=True, description="Overlap initMatirx")
    variant("log_eval", default=False, description="Show logging of evaluator")
    variant("mat_size", default='7', description="Mat size", values=('4','5','6','7'))

    conflicts("~cuda", msg="HyQuas requires CUDA")

    def setup_build_environment(self, env):
        env.set("NCCL_ROOT", self.spec['nccl'].prefix)

    def setup_run_environment(self, env):
        env.set("HYQUAS_ROOT", self.stage.source_path)

    @run_before('cmake')
    def build_cutt(self):
        cuda_arch = self.spec.variants['cuda_arch'].value[0]

        # Filtering CMakefile & Makefile to change the cuda arch flags which are hardcoded
        CMakefile = FileFilter('CMakeLists.txt')
        CMakefile.filter("CUDA_NVCC_FLAGS .*",  "CUDA_NVCC_FLAGS \"-Xcompiler -fopenmp -std=c++14 -O2 -g -arch=compute_{0} -code=sm_{0} --ptxas-options=-v -lineinfo -keep\")".format(cuda_arch))
        with working_dir('./third-party/cutt'):
            makefile = FileFilter('Makefile')
            makefile.filter("GENCODE_FLAGS := .*",  "GENCODE_FLAGS := -gencode arch=compute_{0},code=sm_{0}".format(cuda_arch))

            make()

    def cmake_args(self):
        args = [
            self.define_from_variant('BACKEND', 'backend'),
            self.define_from_variant('SHOW_SUMMARY', 'summary'),
            self.define_from_variant('SHOW_SCHEDULE', 'schedule'),
            self.define_from_variant('MICRO_BENCH', 'micro_bench'),
            self.define_from_variant('USE_DOUBLE', 'use_double'),
            self.define_from_variant('DISABLE_ASSERT', 'disable_assert'),
            self.define_from_variant('ENABLE_OVERLAP', 'overlap'),
            self.define_from_variant('MEASURE_STAGE', 'measure_stage'),
            self.define_from_variant('EVALUATOR_PREPROCESS', 'eval_pp'),
            self.define_from_variant('USE_MPI', 'mpi'),
            self.define_from_variant('OVERLAP_MAT', 'overlap_mat'),
            self.define_from_variant('LOG_EVALUATOR', 'log_eval'),
            self.define_from_variant('MAT', 'mat_size')
        ]

        return args

    def install(self, spec, prefix):
        mkdir(prefix.bin)
        with working_dir(self.stage.source_path):
            mkdirp('evaluator-preprocess/parameter-files')
        with working_dir(self.build_directory):
            install('main', prefix.bin)
