# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.slurm import Slurm as SpackSlurm


class Slurm(SpackSlurm):

    version("24-05-1-1", sha256="8859d90b29e6c81fdc80a16c9063c1ca58fda4245c3835016124cc044a44daec")
    version("23-11-8-1", sha256="0e5aba8eb2b6a5b567a3918190a27d8ae3b60ce03acec225ec7ed2584fa450a1")

    variant("cray_shasta", default=False, description="Enable Cray Shasta support")

    def configure_args(self):
        spec = self.spec

        args=super().configure_args()

        if "+cray_shasta" in spec:
            args.append("--with-cray_shasta")

        return args
