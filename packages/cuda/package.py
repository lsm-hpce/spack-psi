# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack.package import *
from spack.pkg.builtin.cuda import Cuda as SpackCuda


class Cuda(SpackCuda):

    def setup_run_environment(self, env):
        super().setup_run_environment(env)

        if '%nvhpc' in self.spec:
          env.prepend_path("LD_LIBRARY_PATH", self.prefix.lib64)
          env.prepend_path("LD_LIBRARY_PATH", join_path(self.prefix, '../math_libs/lib64'))
