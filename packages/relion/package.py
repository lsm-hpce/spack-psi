# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack.package import *
from spack.pkg.builtin.relion import Relion as SpackRelion


class Relion(SpackRelion):
    version("5.0.0", sha256="800ad0c0aa778cbf584fcf8986976645f2b25d677a80f168e5397975b9db6e47")

    variant("python-exe-path", default="/data/project/bio/hpce/relion-5.0/miniconda3/envs/relion-5.0/bin/python", description="Path to conda env python binary")
    variant("torch-home-path", default="/data/project/bio/hpce/relion-5.0/torch", description="Path to pretrained model weights")

    def cmake_args(self):
        args = super().cmake_args()

        if "@5.0.0" in self.spec:
            args += ["-DPYTHON_EXE_PATH={0}".format(self.spec.variants["python-exe-path"].value)]
            args += ["-DTORCH_HOME_PATH={0}".format(self.spec.variants["torch-home-path"].value)]

        return args
