# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.py_jaxlib import PyJaxlib as SpackPyJaxlib

class PyJaxlib(SpackPyJaxlib):

    version("0.3.25", sha256="73ebc7868631cd9d520385557bbd7f08762d748a5a6a1bebef0f3b8d7ba748ef")
