# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.gams import Gams as SpackGams


class Gams(SpackGams):
    version("48.1", md5="5a59a973edfc4e3170b9535760593d62", expand=False)
    version("49.1", md5="bc9b82d015c9672f8c153003c7a5dd2a", expand=False)
