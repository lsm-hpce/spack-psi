# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install ippl
#
# You can edit this file again by typing:
#
#     spack edit ippl
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *


class Ippl(CMakePackage, CudaPackage):
    """Independent Parallel Particle Layer (IPPL) is a performance portable C++ library for Particle-Mesh methods. IPPL makes use of Kokkos (https://github.com/kokkos/kokkos), HeFFTe (https://github.com/icl-utk-edu/heffte), and MPI (Message Passing Interface) to deliver a portable, massively parallel toolkit for particle-mesh methods. IPPL supports simulations in one to six dimensions, mixed precision, and asynchronous execution in different execution spaces (e.g. CPUs and GPUs)."""

    homepage = "https://github.com/IPPL-framework/ippl"
    url = "https://github.com/IPPL-framework/ippl/archive/refs/tags/IPPL-3.2.0.tar.gz"
    
    git="https://github.com/IPPL-framework/ippl.git"
    
    maintainers("germanne", "gsell")

    license("GPLv3")

    version("3.2.0", sha256="041a4efbddaba5b477dae01ab166354da7dee01cf625706e19f5b60c0c3f6b88")

    version('opalx-fielddump', branch="opalx-fielddump")

    variant(
        "build_type",
        default="Release",
        description="CMake build type",
        values=("Debug", "Release", "RelWithDebInfo", "MinSizeRel"),
    )
    variant("mpi", default=True, description="Enable MPI support")
    variant(
            "platforms", 
            default=True, 
            description="Set IPPL platforms",
            values=("serial", "openmp", "cuda", "openmp/cuda")
    )
    variant("solvers", default=True, description="Enable solvers")
    variant("fft", default=True, description="Enable full-wave solver")
    variant("tests", default=False, description="Enable tests")
    variant("unit_tests", default=False, description="Enable unit tests")
    variant("alternative_variant", default=False, description="Use alternative variant")
    variant("alpine", default=False, description="Enable Alpine")

    depends_on("cmake@3.25.2:", type="build")
    depends_on("fftw@3.3.10", when="+fft")
    depends_on("mpi", when="+mpi")

    conflicts("~mpi", msg="IPPL can not run without mpi!")
    conflicts("cuda_arch=none", when="+cuda",
       msg="CUDA architecture is required")
    conflicts("~cuda", when="platform=cuda",
       msg="CUDA is required for platform cuda.")

    def cmake_args(self):
        args = [
            "-DCMAKE_CXX_STANDARD=20",
            self.define_from_variant("ENABLE_FFT", "fft"),
            self.define_from_variant("ENABLE_SOLVERS", "solvers"),
            self.define_from_variant("ENABLE_TESTS", "tests"),
            self.define_from_variant("ENABLE_UNIT_TESTS", "unit_tests"),
            self.define_from_variant("USE_ALTERNATIVE_VARIANT", "alternative_variant"),
            self.define_from_variant("ENABLE_ALPINE", "alpine"),
        ]

        if 'platforms=cuda' in self.spec:
            args.append("-DIPPL_PLATFORMS=CUDA")
        elif 'platforms=openmp' in self.spec:
            args.append("-DIPPL_PLATFORMS=OPENMP")
        elif 'platforms=openmp/cuda' in self.spec:
            args.append("-DIPPL_PLATFORMS=OPENMP;CUDA")
        else:
            args.append("-DIPPL_PLATFORMS=SERIAL")

        if 'cuda_arch=90' in self.spec:
          args.append("-DKokkos_ARCH_HOPPER90=ON")
        elif 'cuda_arch=80' in self.spec:
          args.append("-DKokkos_ARCH_AMPERE80=ON")
        elif 'cuda_arch=60' in self.spec:
          args.append("-DKokkos_ARCH_PASCAL61=ON")

        return args
