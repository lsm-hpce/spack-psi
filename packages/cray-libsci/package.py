# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.cray_libsci import CrayLibsci as SpackCrayLibsci


class CrayLibsci(SpackCrayLibsci):

    @property
    def external_prefix(self):
        libsci_module = module("show", self.modname).splitlines()

        for line in libsci_module:
            if "CRAY_PE_LIBSCI_PREFIX_DIR" in line:
                return get_path_args_from_module_line(line)[0]
