# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Mithra(MakefilePackage):
    """MITHRA is a full-wave numerical solver for free-electron lasers"""

    homepage = "https://github.com/aryafallahi/mithra/tree/master"
    url = "https://github.com/aryafallahi/mithra/archive/refs/tags/2.0.tar.gz"

    maintainers("germanne", "gsell")

    license("UNKNOWN")

    version("2.0", sha256="66f27ba6ca2c1ab05abb76338d00e661a6fe96b95283b85409c37625b4758deb")

    patch("mithra-fPIC.patch")

    variant("mpi", default=True, description="Enable MPI support")

    depends_on("mpi", when="+mpi")

    conflicts("~mpi")

    def setup_build_environment(self, env):
        env.set("PREFIX", prefix)

    def edit(self, spec, prefix):
        pass
