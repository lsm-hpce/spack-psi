packages:
  all:
    compiler: [gcc, nvhpc, cce]
    prefer:
    - spec: '^openmpi'
      when: '^mpi'
    - spec: '^openblas'
      when: '%gcc ^blas'
    - spec: '^netlib-scalapack'
      when: '+scalapack'
    providers:
      blas: [openblas, nvhpc, nvpl-blas]
      lapack: [openblas, nvhpc, nvpl-lapack]
      mpi: [openmpi, cray-mpich]
      pkgconfig: [pkg-config]
      scalapack: [netlib-scalapack]
    require:
    - spec: "os=sles15"
    - spec: "target=aarch64"
      when: "%gcc"
    - spec: "target=neoverse_v2"
      when: "%nvhpc"
    # Grace Hopper arch
    variants:
      - cuda_arch=90
  berkeley-db:
    require: '%gcc@7.5.0'
  boost:
    require: '%gcc'
  ca-certificates-mozilla:
    require: '%gcc@7.5.0'
  cce:
    externals:
      - modules:
        - libfabric/1.15.2.0
        - PrgEnv-cray/8.5.0
        - cce/17.0.0
        spec: cce@=17.0.0
        extra_attributes:
          compilers:
            c: cc
            cxx: CC
            fortran: ftn
          operating_system: sles15
          target: aarch64
  cmake:
    require: '%gcc@7.5.0'
  coreutils:
    require: '%gcc@7.5.0'
  cray-libsci:
    buildable: false
    externals:
      - modules:
        - cray-libsci
        prefix: /opt/cray/pe/libsci/23.12.5/cray/17.0/aarch64
        spec: cray-libsci@23.12.5%cce@17.0.0 +mpi +openmp
      - modules:
        - cray-libsci
        prefix: /opt/cray/pe/libsci/23.12.5/GNU/12.3/aarch64/
        spec: cray-libsci@23.12.5%gcc@12.3 +mpi +openmp
  cray-mpich:
    buildable: false
    externals:
      - modules:
          - PrgEnv-cce
          - cray-mpich
        prefix: /opt/cray/pe/mpich/8.1.28/ofi/cray/17.0
        spec: cray-mpich@8.1.28%cce@17.0.0
      - modules:
          - PrgEnv-gnu
          - cray-mpich
        prefix: /opt/cray/pe/mpich/8.1.28/ofi/gnu/12.3
        spec: cray-mpich@8.1.28%gcc@12.3
  cray-pmi:
    buildable: false
    externals:
      - modules:
          - cray-pmi
        prefix: /opt/cray/pe/pmi/6.1.13
        spec: cray-pmi@6.1.13%gcc
  cuda:
    prefer:
    - spec: '@12.2.0'
    require: '@12.2.0:'
    externals:
      - modules:
        - nvhpc-nompi/24.3
        prefix: /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/cuda
        spec: cuda@12.2.0%nvhpc@24.3
        extra_attributes:
          environment:
            prepend_path:
              LD_LIBRARY_PATH: /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/math_libs/lib64/stubs
  curl:
    require: '%gcc@7.5.0'
  egl:
    buildable: False
    externals:
      - spec: egl@1.5.0
        prefix: /usr/
  expat:
    require: '%gcc@7.5.0'
  fftw:
    externals:
      - modules:
        - cray-fftw/3.3.10.6
        prefix: /opt/cray/pe/fftw/3.3.10.6/aarch64
        spec: fftw@3.3.10.6+mpi+openmp~pfft_patches precision=double,float%gcc@12.3
  findutils:
    require: '%gcc@7.5.0'
  flex:
    require: '%gcc@7.5.0'
  fltk:
    require: '%gcc@7.5.0'
  fontconfig:
    require: '%gcc@7.5.0'
  gawk:
    require: '%gcc@7.5.0'
  gcc:
    externals:
      - prefix: /usr
        spec: gcc@7.5.0
        extra_attributes:
          compilers:
            c: /usr/bin/gcc
            cxx: /usr/bin/g++
            fortran: /usr/bin/gfortran
          operating_system: sles15
          target: aarch64
        modules:
          - libfabric/1.15.2.0
      - prefix: /usr
        spec: gcc@=12.3
        extra_attributes:
          compilers:
            c: /usr/bin/gcc-12
            cxx: /usr/bin/g++-12
            fortran: /usr/bin/gfortran-12
          operating_system: sles15
          target: aarch64
        modules:
          - libfabric/1.15.2.0
          - PrgEnv-gnu/8.5.0
          - gcc-native/12.3
  gdbm:
    require: '%gcc@7.5.0'
  gettext:
    require: '%gcc@7.5.0'
  git:
    require: '%gcc@7.5.0'
  gmake:
    require: '%gcc@7.5.0'
  groff:
    require: '%gcc@7.5.0'
  libbsd:
    require: '%gcc@7.5.0'
  libevent:
    require: '%gcc@7.5.0'
  # Crayism
  libfabric:
    buildable: true
    externals:
      - modules:
          - libfabric/1.15.2.0
        prefix: /opt/cray/libfabric/1.15.2.0
        spec: libfabric@1.15.2.0
  libiconv:
    require: '%gcc@7.5.0'
  libidn2:
    require: '%gcc@7.5.0'
  libmd:
    require: '%gcc@7.5.0'
  libpciaccess:
    require: '%gcc@7.5.0'
  libsigsegv:
    require: '%gcc@7.5.0'
  librsvg:
    buildable: false
    externals:
      - prefix: /usr
        spec: librsvg@2.48.0
  libtool:
    require: '%gcc@7.5.0'
  libxml2:
    require: '%gcc@7.5.0'
  llvm:
    require: '%gcc@7.5.0'
  m4:
    require: '%gcc@7.5.0'
  munge:
    buildable: false
    externals:
      - prefix: /usr
        spec: munge@0.5.15
  ncurses:
    require: '%gcc@7.5.0'
  netcdf-c:
    externals:
      - modules:
        - cray-netcdf/4.9.0.9
        prefix: /opt/cray/pe/netcdf/4.9.0.9/crayclang/17.0
        spec: netcdf-c@4.9.0.9~dap~hdf4~jna~mpi~parallel-netcdf+shared%cce@17.0.0
      - modules:
        - cray-netcdf-hdf5parallel/4.9.0.9
        prefix: /opt/cray/pe/netcdf-hdf5parallel/4.9.0.9/crayclang/17.0
        spec: netcdf-c@4.9.0.9~dap~hdf4~jna+mpi~parallel-netcdf+shared%cce@17.0.0
      - modules:
        - cray-netcdf/4.9.0.9
        prefix: /opt/cray/pe/netcdf/4.9.0.9/gnu/12.3
        spec: netcdf-c@4.9.0.9~dap~hdf4~jna~mpi~parallel-netcdf+shared%gcc@12.3
      - modules:
        - cray-netcdf-hdf5parallel/4.9.0.9
        prefix: /opt/cray/pe/netcdf-hdf5parallel/4.9.0.9/gnu/12.3
        spec: netcdf-c@4.9.0.9~dap~hdf4~jna+mpi~parallel-netcdf+shared%gcc@12.3
  netcdf-fortran:
    externals:
      - modules:
        - cray-netcdf/4.9.0.9
        prefix: /opt/cray/pe/netcdf/4.9.0.9/crayclang/17.0
        spec: netcdf-fortran@4.9.0.9~doc+shared%cce@17.0.0
      - modules:
        - cray-netcdf/4.9.0.9
        prefix: /opt/cray/pe/netcdf/4.9.0.9/gnu/12.3
        spec: netcdf-fortran@4.9.0.9~doc+shared%gcc@12.3
  ninja:
    require: '%gcc@7.5.0'
  numactl:
    require: '%gcc@7.5.0'
  nvhpc:
    externals:
      - prefix: /opt/nvidia/hpc_sdk
        spec: nvhpc@24.3
        extra_attributes:
          compilers:
            c: /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/compilers/bin/nvc
            cxx: /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/compilers/bin/nvc++
            fortran: /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/compilers/bin/nvfortran
          operating_system: sles15
          modules:
            - "libfabric/1.15.2.0"
            - "nvhpc-nompi/24.3"
          extra_rpaths:
            - /opt/nvidia/hpc_sdk/Linux_aarch64/24.3/math_libs/lib64/stubs
  opal:
    require:
      - spec: '%gcc'
      - message: Opal 2022.1.0 only builds with gcc@10.4.0
        spec: '%gcc@10.4.0 ^openmpi'
        when: '@2022.1.0'
  opengl:
    require: '%gcc@7.5.0'
  openldap:
    buildable: false
    externals:
      - prefix: /usr
        spec: openldap@2.4.46
  openmpi:
    require:
      - spec: "schedulers=slurm fabrics=ofi +cuda"
        when: "@5.0.0:"
      - spec:  "schedulers=slurm fabrics=ofi +cxx +cuda +legacylaunchers"
        when: "@:5.0.0"
  openssh:
    require: '%gcc@7.5.0'
  openssl:
    require: '%gcc@7.5.0'
  parallel-netcdf:
    externals:
      - modules:
        - cray-parallel-netcdf/1.12.3.9
        prefix: /opt/cray/pe/parallel-netcdf/1.12.3.9/crayclang/17.0
        spec: parallel-netcdf@1.12.3.9+cxx+fortran%cce@17.0.0
      - modules:
        - cray-parallel-netcdf/1.12.3.9
        prefix: /opt/cray/pe/parallel-netcdf/1.12.3.9/gnu/12.3
        spec: parallel-netcdf@1.12.3.9+cxx+fortran%gcc@12.3
  perl:
    buildable: false
    externals:
      - prefix: /usr
        spec: perl@5.26.1
  pkg-config:
    require: '%gcc@7.5.0'
  pkgconf:
    require: '%gcc@7.5.0'
  pmix:
    require: '+munge'
  py-fypp:
    require: '%gcc@7.5.0'
  python:
    require: '%gcc@7.5.0'
  quantum-espresso:
    require:
      - spec: "^nvpl-blas ^nvpl-lapack"
        when: "%nvhpc"
    conflict:
    - spec: '^acfl'
      when: '%nvhpc'
  rdma-core:
    require: '%gcc@7.5.0'
  readline:
    require: '%gcc@7.5.0'
  slurm:
    buildable: false
    externals:
      - prefix: /usr
        spec: slurm@24.05.3%gcc@7.5.0 +cgroup+gtk+hwloc+mariadb+nvml+pam+pmix+restd
  sqlite:
    require: '%gcc@7.5.0'
  tar:
    require: '%gcc@7.5.0'
  texinfo:
    require: '%gcc@7.5.0'
  unuran:
    require: '%gcc@7.5.0'
  util-linux-uuid:
    require: '%gcc@7.5.0'
  util-macros:
    require: '%gcc@7.5.0'
  xpmem:
    buildable: false
    externals:
      - spec: xpmem@2.9.6-1.1
        prefix: /usr
        modules:
        - xpmem/2.9.6-1.1_20240511212657__g087dc11fc19d
  xxhash:
    require: '%gcc@7.5.0'
  xz:
    externals:
      - prefix: /usr
        spec: xz@5.2.3
  zlib:
    require: '%gcc@7.5.0'
  zstd:
    require: '%gcc@7.5.0'
