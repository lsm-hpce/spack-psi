# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.py_chex import PyChex as SpackPyChex

class PyChex(SpackPyChex):

    version("0.0.7", sha256="4ea31fbf17075f376040ac9ce3f10de94cefa8c37a974ca0871b4260927aa924")
