# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyProjectq(PythonPackage):
    """
    ProjectQ is an open-source software framework for quantum computing started
    at ETH Zurich. It allows users to implement their quantum programs in
    Python using a powerful and intuitive syntax. ProjectQ can then translate
    these programs to any type of back-end, be it a simulator run on a
    classical computer of an actual quantum chip.
    """

    # Homepage and git repository
    homepage = "https://projectq.ch"

    url = 'https://github.com/ProjectQ-Framework/ProjectQ/archive/refs/tags/v0.4.1.tar.gz'
    git = 'https://github.com/ProjectQ-Framework/ProjectQ.git'

    # Versions
    version("develop", branch="develop")
    version("0.3.6", commit="fa484fe037a3a1772127bbd00fe4628ddba34611")
    version("0.4.1", sha256="008f39f9e9cda47dde97020f640f89a9ee1488d37ab45d8fad78c112b15e695d")
    version("0.6.1", sha256="8d8f8694c49b85714957d6db9154af3b2f769133256157435fbc103c2dbcf1ae")

    # Dependencies
    depends_on("py-setuptools", type=("build"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-scipy", type=("build", "run"))
    depends_on("py-future", type=("build", "run"))
    depends_on("py-requests", type=("build", "run"))
    # conflict with pybind11@2.2.0 -> see requirements.txt
    depends_on("py-pybind11@1.7:2.1,2.2.1:", type=("build", "run"))
    depends_on("py-networkx", type=("run"))
