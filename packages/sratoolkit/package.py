# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.sratoolkit import Sratoolkit as SpackSratoolkit


class Sratoolkit(SpackSratoolkit):

    version("3.1.1", sha256="600ff38589c15033c6d0fb4d1d055519e56175ea031b949a2d3f5a74151c6c12")
