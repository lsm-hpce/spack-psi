# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Opal(CMakePackage):
    """OPAL (Object Oriented Parallel Accelerator Library) is a parallel open source tool for charged-particle optics in linear accelerators and rings,
    including 3D space charge. Using the MAD language with extensions, OPAL can run on a laptop as well as on the largest high performance computing systems.
    OPAL is built from the ground up as a parallel application exemplifying the fact that high performance computing is the third leg of science,
    complementing theory and experiment."""

    homepage = "https://amas.web.psi.ch/opal/Documentation/master/OPAL_Manual.html"
    url = "https://gitlab.psi.ch/OPAL/src/-/archive/OPAL-2022.1.0/src-OPAL-2022.1.0.tar.gz"
    git = "https://gitlab.psi.ch/OPAL/src.git"

    maintainers("germanne", "gsell")

    license("GPLv3")

    version('master', branch="master")
    version("2022.1.0", sha256="31f6811032e9d5400169d28a8ae6c7dee2e4a803ee82e90584d31ff7a0a18d80")
    version("2024.1.0", sha256="65c4c263d6394b4b234323a74fc55eb963fb75cb80ea904d6c82054cfc64033c")

    variant(
        "build_type",
        default="Release",
        description="CMake build type",
        values=("Debug", "Release", "RelWithDebInfo", "MinSizeRel"),
    )
    variant("mpi", default=True, description="Enable MPI support")
    variant("tests", default=True, description="Enable OPAL unit-tests")
    variant("amr", default=True, description="Enable AMR solver")
    variant("amr_mg_solver", default=True, description="Enable AMR multigrid solver")
    variant("static", default=True, description="Enable MPI support")
    variant("bandrf", default=True, description="Compile BANDRF field conversion scripts")
    variant("static", default=True, description="Enable MPI support")
    variant("opal_fel", default=True, description="Enable full-wave solver")
    variant("saamg_solver", default=True, description="Enable SAAMG solver")
    variant("python", default=True, description="Enable PyOPAL")
    variant("mslang", default=True, description="Enable mslang")
    variant("ippltests", default=True, description="Enable IPPL tests")

    depends_on("blas")
    depends_on("boost+chrono+filesystem+iostreams+regex+serialization+system+timer")
    depends_on("boost+mpi", when="+mpi")
    depends_on("boost+python+shared", when="+python")
    depends_on("python@3.0:", when="+python")
    depends_on(
        "boost+mpi+chrono+filesystem+iostreams+regex+serialization+system+timer", when="+mpi"
    )
    depends_on("gsl+shared")

    depends_on("h5hut+mpi", when="+mpi")
    depends_on("h5hut~mpi", when="~mpi")

    depends_on(
        "amrex@18.07 precision=double dimensions=3 +mpi~openmp+particles+pic~eb",
        when="+amr+amr_mg_solver+mpi",
    )
    depends_on("googletest@1.10.0: +shared", when="+tests")
    depends_on("mithra@2.0", when="+opal_fel")
    depends_on(
        "trilinos@12.0.1: cxxstd=17 +mpi+muelu+nox+zoltan+zoltan2+isorropia+shared gotype=long",
        when="+saamg_solver",
    )
    depends_on(
        "trilinos@12.0.1: cxxstd=17 +mpi+muelu+nox+zoltan+zoltan2+isorropia+shared gotype=long",
        when="+amr_mg_solver",
    )
    depends_on("parmetis@4.0.3:", when="+saamg_solver")
    depends_on("parmetis@4.0.3:", when="+amr_mg_solver")

    # @master
    depends_on("h5hut@2.0.0rc6", when="@master")
    depends_on("trilinos@13.4.0: +shared", when="@master+saamg_solver")
    depends_on("trilinos@13.4.0: +shared", when="@master+amr_mg_solver")

    # @2022.1%gcc@10.4.0
    depends_on("h5hut@2.0.0rc6", when="@2022.1")
    depends_on("trilinos@13.4.0", when="@2022.1+saamg_solver")
    depends_on("trilinos@13.4.0", when="@2022.1+amr_mg_solver")

    conflicts("~amr", "+amr_mg_solver")
    conflicts("~mpi", msg="OPAL can not run without mpi!")

    # AMRex version is too old for any newer Trilinos version
    requires(
        "^trilinos@13.4.0",
        when="^amrex@18.07",
        msg="AMRex version is too old for any newer Trilinos or GCC versions."
    )

    def cmake_args(self):
        if "+mpi" in self.spec:
            env["CC"] = self.spec["mpi"].mpicc
            env["CXX"] = self.spec["mpi"].mpicxx
            env["FC"] = self.spec["mpi"].mpifc

        if "+opal_fel" in self.spec:
            env["MITHRA_PREFIX"] = self.spec["mithra"].prefix

        args = [
            self.define_from_variant("BUILD_OPAL_UNIT_TESTS", "tests"),
            self.define_from_variant("ENABLE_AMR", "amr"),
            self.define_from_variant("ENABLE_AMR_MG_SOLVER", "amr_mg_solver"),
            self.define_from_variant("ENABLE_BANDRF", "bandrf"),
            self.define_from_variant("ENABLE_OPAL_FEL", "opal_fel"),
            self.define_from_variant("ENABLE_SAAMG_SOLVER", "saamg_solver"),
            self.define_from_variant("BUILD_OPAL_PYTHON", "python"),
            self.define_from_variant("WILL_BUILD_SHARED_LIBRARY", "python"),
            self.define_from_variant("ENABLE_IPPLTESTS", "ippltests"),
            self.define_from_variant("ENABLE_MSLANG", "mslang"),
        ]

        return args
