# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install opal-x
#
# You can edit this file again by typing:
#
#     spack edit opal-x
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *


class OpalX(CMakePackage, CudaPackage):
    """OPAL (Object Oriented Parallel Accelerator Library) is a parallel open source tool for charged-particle optics in linear accelerators and rings,
    including 3D space charge. Using the MAD language with extensions, OPAL can run on a laptop as well as on the largest high performance computing systems.
    OPAL is built from the ground up as a parallel application exemplifying the fact that high performance computing is the third leg of science,
    complementing theory and experiment."""

    homepage = "https://amas.web.psi.ch/opal/Documentation/master/OPAL_Manual.html"
    git = "https://gitlab.psi.ch/OPAL/opal-x/src.git"

    maintainers("germanne", "gsell")

    license("GPLv3")

    version('master', branch="master")
    version('fixSolverUnits', branch="fixSolverUnits")

    variant(
        "build_type",
        default="Release",
        description="CMake build type",
        values=("Debug", "Release", "RelWithDebInfo", "MinSizeRel"),
    )
    variant("mpi", default=True, description="Enable MPI support")
    variant("solvers", default=True, description="Enable solvers")
    variant("fft", default=True, description="Enable full-wave solver")
    variant("tests", default=False, description="Enable tests")
    variant("alternative_variant", default=False, description="Use alternative variant")
    variant("alpine", default=False, description="Enable Alpine")
    variant("unit_tests", default=False, description="Build unit tests")
    variant("ippl_git_tag", default="master", description="IPPL git tag")

    depends_on("blas")
    depends_on("boost@1.82.0+mpi+chrono+filesystem+iostreams+regex+serialization+system+timer+python+shared")
    depends_on("cmake@3.25.2:", type="build")
    depends_on("fftw@3.3.10 +mpi")
    depends_on("gsl@2.7+shared")
    depends_on("gnutls@3.5.19:")
    depends_on("cuda@12.4.0:")

    depends_on("h5hut+mpi", when="+mpi")

    depends_on("googletest@1.13.0: +shared", when="+tests")
    # @master
    depends_on("h5hut@2.0.0rc6:", when="@master")

    # @2022.1%gcc@10.4.0
    depends_on("h5hut@2.0.0rc6", when="@2022.1")

    conflicts("~mpi", msg="OPAL can not run without mpi!")
    conflicts("cuda_arch=none", when="+cuda",
          msg="CUDA architecture is required")
    conflicts("%gcc@:8.5.0", msg="gcc bigger than 8.5.0 is required")

    @run_before("cmake")
    def cmake_configure(self):
      spec = self.spec
      cmake_configure = Executable("./gen_OPALrevision")
      cmake_configure()

    def cmake_args(self):
        args = [
            "-DCMAKE_CXX_STANDARD=20",
            self.define_from_variant("ENABLE_FFT", "fft"),
            self.define_from_variant("ENABLE_SOLVERS", "solvers"),
            self.define_from_variant("ENABLE_TESTS", "tests"),
            self.define_from_variant("USE_ALTERNATIVE_VARIANT", "alternative_variant"),
            self.define_from_variant("ENABLE_ALPINE", "alpine"),
            self.define_from_variant("ENABLE_UNIT_TESTS", "unit_tests"),
            "-DIPPL_GIT_TAG=opalx-fielddump",
            "-DKokkos_ENABLE_IMPL_CUDA_MALLOC_ASYNC=OFF",
        ]

        if '+cuda' in self.spec:
            args.append("-DIPPL_PLATFORMS=CUDA")
        else:
            args.append("-DIPPL_PLATFORMS=openmp")

        if 'cuda_arch=90' in self.spec:
          args.append("-DKokkos_ARCH_HOPPER90=ON")
        elif 'cuda_arch=80' in self.spec:
          args.append("-DKokkos_ARCH_AMPERE80=ON")
        elif 'cuda_arch=60' in self.spec:
          args.append("-DKokkos_ARCH_PASCAL61=ON")


        return args

