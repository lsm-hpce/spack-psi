# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.py_absl_py import PyAbslPy as SpackPyAbslPy

class PyAbslPy(SpackPyAbslPy):

    version("1.0.0", sha256="ac511215c01ee9ae47b19716599e8ccfa746f2e18de72bdf641b79b22afa27ea")
