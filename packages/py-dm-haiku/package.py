# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack.package import *
from spack.pkg.builtin.py_dm_haiku import PyDmHaiku as SpackPyDmHaiku

class PyDmHaiku(SpackPyDmHaiku):

    version("0.0.9", sha256="97752b32cdbe5a3e2d1c60ea884d33eb4b36e7d410000f0d61b571417c925435")
